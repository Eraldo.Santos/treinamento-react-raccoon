import React from 'react';
import { FooterBase } from './styles';

function Footer() {
  return (
    <FooterBase>
      <p>Eraldo dos Santos Neto</p>
      <p>
        Treinamento de React
      </p>
    </FooterBase>
  );
}

export default Footer;
