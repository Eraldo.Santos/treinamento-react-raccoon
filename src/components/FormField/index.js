import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function FormField({ label, type, name, value, onChange }) {

    const fieldId = `id_${name}`;

    const isTypeTextArea = type === 'textarea';

    const tag = isTypeTextArea ? 'textarea' : 'input';

    const Input = styled.input`
    
    `;

    return (
        <div>
            <label htmlFor={fieldId}>
                {label}
                <Input 
                    as={tag}
                    type={type} 
                    name={name}
                    value={value}
                    id={fieldId}
                    onChange={onChange}
                />
            </label>
        </div>
    );
}

FormField.defaultProps = {
    type: 'text',
    value: '',
    onChange: () => {}
}

FormField.propTypes = {
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
}

export default FormField;