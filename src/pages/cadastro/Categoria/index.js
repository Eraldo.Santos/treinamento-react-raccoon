import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../../../components/Button';
import FormField from '../../../components/FormField';
import PageDefault from '../../../components/PageDefault';

function CadastroCategoria() {
    return (
        <PageDefault>
            <h1>Cadastro de Categoria</h1>

            <form>
                <FormField label="Nome da Categoria"
                name="nome"
                />

                <FormField
                    label="Descrição"
                    type="textarea"
                    name="descricao"
                />

                <FormField
                    label="Cor"
                    type="color"
                    name="cor"
                />

                <FormField
                    label="Radio"
                    type="radio"
                    name="radio"
                />

            <Button>Cadastrar</Button>

            </form>

            <Link to="/">
                Ir para Home
            </Link>
        </PageDefault>
    )
}

export default CadastroCategoria